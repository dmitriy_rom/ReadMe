class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_filter :detect_is_devise
  protect_from_forgery with: :exception

  # HTTP basic authentication
  # if %w(production staging).include?(Rails.env)
  #   http_basic_authenticate_with name: "proekt", password: "palenin"
  # end
  private

  def detect_is_devise
    if devise_controller?
      @is_devise=true
    else
      @is_devise=false
    end
    return @is_devise
  end

end
